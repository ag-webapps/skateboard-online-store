require("dotenv").config();

const initialSkateboards = require("./skateboards.json");

const { name } = require("ejs");

const express = require("express");
const session = require("express-session");
const cookieParser = require("cookie-parser");

const { MongoClient, ServerApiVersion } = require("mongodb");
const dbUrl = "mongodb://localhost:27017";

if (!dbUrl) throw `MONGODB_URL not found!`;

const app = express();

const mongo = new MongoClient(dbUrl, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true
  }
});

const db = mongo.db("advanced-internet-applications").collection("skateboards");

let badRequest = (res, msg) => {
  res.status(400).send(msg);
};

let skateboards = [];
async function initDb() {
  try {
    await mongo.connect();
    await db.deleteMany({});
    await db.insertMany(initialSkateboards.skateboards);
    skateboards = await db.find().toArray();
  } catch (err) {
    console.error(err);
  }
}

app.use(express.static('public'))
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  session({
    secret: "secret-key",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
  })
);
app.use(cookieParser());
app.set("view engine", "ejs");


// Define a route
app.get("/", async (req, res) => {
  try {
    res.render("pages/index", { skateboards: skateboards, cart: req.session.cart ?? [] });
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error: home page");
  }
});

app.post("/add-to-cart", async (req, res) => {
  if (!req.body.itemId) {
    badRequest(res, "No item specified to be added to the cart");
    return;
  }
  try {
    if (!req.session.cart)
      req.session.cart = [];

    const newSkateboard = await db.findOne({ id: req.body.itemId });
    if (newSkateboard === null) {
      res.status(500).send("Server Error: Requested item not available");
      return;
    }
    req.session.cart.push(newSkateboard);
    
    res.redirect('back');
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error: adding item to cart");
  }
});

app.post("/cancel-checkout", async (req, res) => {
  try {
    req.session.cart = [];    
    res.redirect('back');
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error: cencelling checkout");
  }
});

app.post("/remove-from-cart", async (req, res) => {
  if (!req.body.itemId) {
    badRequest(res, "No item specified to be removed from the cart");
    return;
  }
  if (!req.session.cart) {
    badRequest(res, "Cart is empty");
    return;
  }
  try {
    req.session.cart = req.session.cart.filter(
      sb => sb.id !== req.body.itemId
    );
    res.redirect("back");
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error: removing item from cart");
  }
});

app.get("/cart", (req, res) => {
  if (!req.session.cart) req.session.cart = [];
  res.render("pages/cart", {cart: req.session.cart});
});

function containsAll(arr, items) {
  for (const item of items) {
    if (!arr.some(i => i.id === item.id))
      return false;
  }
  return true;
}

app.post("/checkout", async (req, res) => {
  if (!req.session.cart || req.session.cart.length === 0) {
    badRequest(res, "Cart is empty");
    return;
  }
  try {
    skateboards = await db.find().toArray();
    if (!containsAll(skateboards, req.session.cart)) {
      req.session.cart = [];
      res.redirect("/purchase-cancelled");
      return;
    }
    const idsToDelete = req.session.cart.map((sb) => sb.id);
    req.session.cart = [];
    await db.deleteMany({id: { $in: idsToDelete}});
    skateboards = await db.find().toArray();
    res.redirect("/purchase-completed");
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error. Cannot display checkout.");
  }
});

app.get("/purchase-completed", (req, res) => {
  res.render("pages/purchase-completed");
})

app.get("/purchase-cancelled", (req, res) => {
  res.render("pages/purchase-cancelled");
})

app.listen(3000, async () => {
  await initDb();
  console.log('Server is running!');
  console.log('http://localhost:3000');
});
